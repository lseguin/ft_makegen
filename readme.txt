ft_makegen create by lseguin @ 42

You can use ft_makegen everywhere :D
create your config file for ft_makegen by default is ~/.makegenrc

/*
** man_makegen
** lionel.seguin@student.42.fr
** 01/2014
*/

Welcome to ft_makegen man


> How to:

Use it in the folder where your project is, it will link any library located
in a sub-folder.


> How to make a library:

If no main is found the program will ask you to create a Makefile for a library.


> How to use config file:

At first launch ft_makegen create a config file located and named "~/.makegenrc"
You can add lib and includes, makegen will add them to compilation rules.

In ~/.makegenrc add:
	flag1_I = -I ...
	flag1_L = -L ... -l ...
	flag2_I = -I ...
	flag2_L = ...
	[...]

Use:
	ft_makegen flag1 flag2 ...

**Example:

mlx_I=-I /usr/X11/include/
mlx_L=-L /usr/X11/lib -lmlx -lXext -lX11

ft_makegen mlx


		/!\_____ WARNNIG ______/!\			
--------------------------------------------
Your project must be create with this format
--------------------------------------------

/your_project/*.c
/your_project/*.h
/your_project/libname/*.c
/your_project/libname/Makefile
/your_project/libname/includes/*.h

